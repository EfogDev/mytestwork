angular.module('testwork')

    .directive('calendar', function () {
        return {
            restrict: 'E',
            scope: {
                events: '&'
            },
            templateUrl: 'templates/calendar.html',
            controller: 'CalendarCtrl'
        }
    })

;