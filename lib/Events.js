class Events {
    constructor(events) {
        this.eventBlocks = [];

        this.events = _.sortBy(events, 'start');

        _.each(this.events, event => {
            this.addEvent(event);
        });
    }

    addEvent(event) {
        if (this.eventBlocks.length === 0) {
            event.layout = {totalColumns: 2, columnsCount: 2, startColumn: 1};
            this.eventBlocks.push(event);
            return;
        }

        var eventConflicts = this.findConflicts(event);

        if (eventConflicts.length == 0) {
            event.layout = {totalColumns: 2, columnsCount: 2, startColumn: 1};
            this.eventBlocks.push(event);
            return;
        }

        var freeColumns = this.findFreeColumnsForEvent(event);

        if (freeColumns) {
            event.layout = {totalColumns: freeColumns.totalColumns, columnsCount: freeColumns.end - freeColumns.start, startColumn: freeColumns.start};
            this.eventBlocks.push(event);

            return;
        }

        var totalColumns;
        var columnsCount;
        var startColumn;

        eventConflicts.forEach(conflict => {
            totalColumns = conflict.layout.totalColumns + 2;
            columnsCount = conflict.layout.columnsCount;
            startColumn = conflict.layout.startColumn;
            conflict.layout.totalColumns = totalColumns;
        });

        event.layout = {totalColumns: totalColumns, columnsCount: columnsCount, startColumn: startColumn + columnsCount};

        this.eventBlocks.push(event);
    }

    findFreeColumnsForEvent(event) {
        return this.findFreeColumns(event.start, event.start + event.duration);
    }

    findFreeColumns(start, end) {
        var events =  this.findEventsByPeriod(start, end);

        var occupiedColumns = [];
        var freeColumns = null;

        _.each(events, event => {
            occupiedColumns.push(event.layout);
        });

        for (var i = 0; i < occupiedColumns.length; i++) {
            var currentPeriod = occupiedColumns[i];
            var freePeriod = [];

            if (i == 0 && currentPeriod.startColumn > 1) {
                freePeriod = {start: 1, end: currentPeriod.startColumn, totalColumns: currentPeriod.totalColumns};

                if (freePeriod.start >= freePeriod.end)
                    return freeColumns;

                return freePeriod;
            }

            if (!occupiedColumns[i + 1]) {
                freePeriod = {start: currentPeriod.startColumn + currentPeriod.columnsCount, end: currentPeriod.totalColumns, totalColumns: currentPeriod.totalColumns};

                if (freePeriod.start >= freePeriod.end)
                    return freeColumns;

                return freePeriod;
            }

            freePeriod = {start: currentPeriod.startColumn + currentPeriod.columnsCount, end: occupiedColumns[i + 1].startColumn, totalColumns: currentPeriod.totalColumns};

            if (freePeriod.start >= freePeriod.end)
                return freeColumns;
        }

        return freeColumns;
    }

    findConflicts(event) {
        return this.findEventsByPeriod(event.start, event.start + event.duration);
    }

    findEventsByPeriod(start, end) {
        var found = [];

        this.eventBlocks.forEach(event => {
            if (Events.isEventInPeriod(event, start, end)) {
                found.push(event);
            }
        });

        return _.sortBy(found, item => item.layout.startColumn);;
    }

    static isEventInPeriod(event, startTime, endTime) {
        var time = {
            start: event.start,
            end: event.start + event.duration
        };

        return (time.end >= startTime && time.end <= endTime) ||
            (time.start >= startTime && time.start <= endTime) ||
            (time.start <= startTime && time.end >=  endTime)
            ;
    };

    getLayout() {
        return this.eventBlocks;
    }
}
